        <footer class="py-5">
            <div class="container py-lg-3">
                <div class="row">
                    <div class="col-sm-4">
                        <img src="assets/images/logo-light.png" alt="" class="" width="auto" height="40px">
                        <h6 class="my-4">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis rhoncus nulla.
                        </h6>
                        <div class="social-media">
                            <img src="assets/images/twitter.png">
                            <img src="assets/images/fb.png">
                            <img src="assets/images/yt.png">
                            <img src="assets/images/ig.png">
                        </div>
                        <p class="my-4">Copyright &copy; 2018 fimela.com <br>KLY KapanLagi Youniverse <br>All Rights Reserved</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>ABOUT</h5>
                            <a href="#"><p>our team</p></a>
                            <a href="#"><p>masthead</p></a>
                            <a href="#"><p>terms</p></a>
                            <a href="#"><p>site map</p></a>
                        </div>
                        <div class="col-sm-6">
                            <h5>PARTNERSHIP</h5>
                            <a href="#"><p>media kit</p></a>
                            <a href="#"><p>career</p></a>
                            <a href="#"><p>advertise</p></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                        <h5>NETWORKING</h5>
                    <div class="row">
                        <div class="col-sm-6 w-100">
                            <a href="#"><p>liputan6.com</p></a>
                            <a href="#"><p>bintang.com</p></a>
                            <a href="#"><p>bola.com</p></a>
                            <a href="#"><p>bola.net</p></a>
                            <a href="#"><p>brilio.net</p></a>
                            <a href="#"><p>famous.id</p></a>
                        </div>
                        <div class="col-sm-6">
                            <a href="#"><p>fimela.com</p></a>
                            <a href="#"><p>kapanlagi.com</p></a>
                            <a href="#"><p>merdeka.com</p></a>
                            <a href="#"><p>otosia.com</p></a>
                            <a href="#"><p>vemale.com</p></a>
                            <a href="#"><p>dream.co.id</p></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
