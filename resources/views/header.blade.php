<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no" />
    <title>Fimela</title>
    <link rel="shortcut icon" href="assets/images/favicon.png">
  
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-grid.min.css">
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/plugins/slick/slick.css" rel="stylesheet">
    <link href="assets/plugins/slick/slick.scss" rel="stylesheet">
    <link href="assets/plugins/slick/slick-theme.scss" rel="stylesheet">
    <link href="assets/plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="assets/plugins/feather-icons/feather.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <!-- JS -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick/slick.min.js"></script>
    <script src="assets/plugins/slick/slick.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<body>
    
<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand text-center" href="#">
    <img src="assets/images/logo.png" width="aut0" height="30" alt="">
  </a>
  <button class="navbar-toggler mobile-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Category
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Skin Care <i class="fa fa-chevron-right"></i></a>
          <a class="dropdown-item" href="#">Make-Up <i class="fa fa-chevron-right"></i></a>
          <a class="dropdown-item" href="#">Body Care <i class="fa fa-chevron-right"></i></a>
          <a class="dropdown-item" href="#">Hair Care <i class="fa fa-chevron-right"></i></a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Brand</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Skin Type</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Age</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>