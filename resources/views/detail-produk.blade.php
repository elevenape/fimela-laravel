<?php include 'header.php';?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Beauty</a></li>
    <li class="breadcrumb-item"><a href="#">Moisturizer</a></li>
    <li class="breadcrumb-item active" aria-current="page">Moisture </li>
  </ol>
</nav>

<div class="vendor text-center">
	<p>supported by</p>
	<img src="assets/images/treva.png">
</div>
<div class="produk">
	<div class="p-4">
	  <div class="produk-box single-item text-center">
	  	<div>
	  		<img src="assets/images/produk.png"  width="200px">
	  	</div>
	  	<div>
	  		<img src="assets/images/produk.png"  width="200px">
	  	</div>
	  	<div>
	  		<img src="assets/images/produk.png"  width="200px">
	  	</div>
	  </div>
	  <div class="title text-center my-4">
	  	<h6 style="color: #ccc;">GARNIER</h6>
	  	<h4 class="my-3">Moisture Bomb The Antioxidant Super Moisturizer SPF 30</h4>
	  	<div class="row">
	  		<div class="col-6 text-right"><h6 style="color: crimson">By Dina Puteri</h6></div>
	  		<div class="col-6 text-left">
	  			<img src="assets/images/Path-1.png" width="20px">
	  			<img src="assets/images/Path-1.png" width="20px">
	  			<img src="assets/images/Path-1.png" width="20px">
	  			<img src="assets/images/Path-1.png" width="20px">
	  			<img src="assets/images/Path.png" width="20px">
	  		</div>
	  	</div>
	  	<h6 style="color: #ccc;">Dina Puteri Is 19-24 Age, Oily & Fair Skin</h6>
	  </div>
	  <p class="my-5" style="font-size: 18px; line-height: 35px">Mengulas gaya selebriti paling fashionable ini, Rihanna! Tampaknya selebriti slash fashion icon ini makin nyentrik dan terlihat one in a million. Pada hari terakhir Coachella kemarin, Rihanna kedapatan tampil super nyentrik dengan topeng rajutan yang menutupi semua. Mengulas gaya selebriti paling fashionable ini.
	  	<br><br>
		Rihanna! Tampaknya selebriti slash fashion icon ini makin nyentrik dan terlihat one in a million. Pada hari terakhir Coachella kemarin, Rihanna kedapatan tampil super nyentrik dengan topeng rajutan yang menutupi semua</p>
		<button class="btn btn-outline-secondary">SKIN CARE</button>
		<button class="btn btn-outline-secondary">MOISTURIZER</button>
		<div class="social-media mt-3 ">
			<img src="assets/images/fb_icon.png" width="70px">
			<img src="assets/images/twitter_icon.png" width="70px">
			<img src="assets/images/bbm_icon.png" width="55px" class="mx-1">
			<img src="assets/images/wa_icon.png" width="60px" class="mx-1">
		</div>
		<div class="produk_title mt-5 mb-3  text-left"><h5 class="produk_title-header" style="display: table-row;"> SMILIAR PRODUCTS </h5></div>
		<div class="">
			<div class="slide-produk">
				<div class="media media-similar">
				  <img src="assets/images/produk-2.jpg" class="mr-3" >
				  <div class="media-body">
				  	<p>WARDAH</p>
				    <h6 class="mt-n2">Perfectcurl Mascara</h6>
				    <p><img src="assets/images/Path-1.png"> 25K</p>
				  </div>
				</div>
				<div class="media media-similar">
				  <img src="assets/images/produk-2.jpg" class="mr-3" >
				  <div class="media-body">
				  	<p>WARDAH</p>
				    <h6 class="mt-n2">Perfectcurl Mascara</h6>
				    <p><img src="assets/images/Path-1.png"> 25K</p>
				  </div>
				</div>
				<div class="media media-similar">
				  <img src="assets/images/produk-2.jpg" class="mr-3" >
				  <div class="media-body">
				  	<p>WARDAH</p>
				    <h6 class="mt-n2">Perfectcurl Mascara</h6>
				    <p><img src="assets/images/Path-1.png"> 25K</p>
				  </div>
				</div>
			</div>
			<button class="btn btn-danger mr-2" style="width: 170px"> REVIEW </button>
			<button class="btn btn-outline-secondary ml-2" style="width: 170px"><i class="fa fa-heart"></i> LIKE</button>
		</div>
		<div class="produk_title mt-5 mb-3  text-left"><h5 class="produk_title-header"> USER REVIEW </h5></div>
		<div class="review p-4">
			<div class="media">
			  <img src="assets/images/foto.png" class="mr-3" alt="..." width="60px">
			  <div class="media-body">
			    <h5 class="mt-0">Media heading</h5>
			    <p><i class="fa fa-clock-o"></i> Pemakaian : 3 bulan - 6 bulan</p>
			  </div>
			</div>
			<p>Pertama kali tahu sunscreen ini dari salah satu beauty vlogger, karena penasaran akhirnya saya beli. Dengan claimnya yang water based jadi terasa sangat ringan di wajah, dikulit ku overall cocok dan gk menimbulkan tumbuhnya jerawat. Sudah pakai selama kurang lebih 1,5 tahun, suka banget!!</p>
			<button class="btn btn-outline-secondary"><i class="fa fa-heart"></i> 25K</button>
		</div>	
	</div>

</div>

<?php include 'footer.php';?>

<script type="text/javascript">
	$('.single-item').slick();
	$('.slide-produk').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  centerMode: false,
	  variableWidth: true
	});
</script>